let button;
let bg1;
let bg2;
let mic;
let capture;
let tos = 'You might as well stop reading here, this is going to be a long text, so in short it is just the typical offical policy stuff to ensure you will not sue us, nothing important you really care about anyway, so just hit agree.                                                                                                                                                                                                                                                                                           BY AGRREEING TO OUR TERMS AND SERVICE, WE WILL TAKE YOUR DATA AND USE IT FOR OUR OWN COMMERICAL GAINS. WE WILL COLLECT ANYTHING WE CAN ABOUT YOU, YOUR MOUSE CLICKS, KEYBOARD CLICKS, YOUR VOICE, YOUR FACE, YOUR HAIR DATA, EYE COLOR, BROWSER HISTORY, ANYTHING YOU SAY WILL BE RECORDED AND STORED FOR USE, YOUR VOICE CADENCE WILL BE SOLD TO THIRD PARTIES THAT WE DO NOT HAVE ANY CLUE WHAT THEY WILL DO WITH, HOWEVER THE PROFIT WE GET FROM YOUR DATA IS GINORMOUS. WE WILL TRY AND CREATE AN ACCURATE PROFILE OF YOU AND YOUR PERSONALITY, SO WE CAN MORE EASILY FIGURE OUT EXACTLY WHAT WOULD GET YOU TO SPEND MONEY, WE WILL LOOK OUT FOR ANY VULNERABILITIES YOU HAVE. YOU WILL BE MANIPULATED INTO BUYING A PRODUCT AND FURTHER FILLING OUR POCKETS WHILE YOU LIVE IN IGNORANT BLISS, IT IS JUST SOME DATA AFTER ALL, WHO CARES ABOUT PRIVACY ANYWAY. WE CAN AND WILL GIVE ANY DATA WE HAVE ON YOU TO AUTHORITIES IF ASKED, THIS DATA CAN AND WILL BE USED TO PROSECUTE OR PREVENT ANY ILLEGAL ACTIVITIES, THAT DOES NOT REALLY MATTER THOUGH DOES IT? YOU ARE A GOOD PERSON AFTERALL, RIGHT? YOU DO NOT HAVE ANYTHING TO HIDE, SO WHY CARE ABOUT YOUR INTERNET PRIVACY AND THE AMOUNT OF DATA WE HAVE OF YOU, ALL OF IT WILL ONE HUNDRED PERCENT BE SOLD TO THIRD PARTIES FOR A BETTER USER EXPERIENCE WHERE WE WILL MORE EASILY BE ABLE TO ADVERISTE PRODUCTS YOU DID NOT KNOW YOU NEEDED, HOWEVER WE DID BECAUSE OF OUR ACCURATE PROFILE OF YOU. DOES THAT NOT SOUND LIKE A GOOD DEAL TO YOU? SO, WHY DONT YOU DO US A FAVOR AND HIT THAT AGREE BUTTON AND YOU WILL GET YOUR COUPLE OF MINUTES ENJOYMENT WORTH OF OUR SERVICE, I MEAN IT IS NOT LIKE YOU ARE SELLING YOUR SOUL, RIGHT?';
let disagreeSize1;
let disagreeSize2;
let x, y;
let blink = 0;
let sceneNum = 0; //sets the first scene as number 0
let loadingBar = 0; //starting position
let countMouse = 0;
let countKey = 0;

function preload(){ //loads in the shadow figure backgrounds
bg1 = loadImage('assets/background1.png'); //Thinker form
bg2 = loadImage('assets/background2.png'); //Menancing form
}

function setup() {
  createCanvas(1000, 700);
  //The agree button creation and styling
  button = createButton("Agree");
  button.position(250, 555);
  button.size(180, 70);
  button.style("background-color","#4491E2");
  button.style("background", "linear-gradient(to bottom, #4491E2 0%, #307CCB 100%)");
  button.style("color", "#ffffff");
  button.style("font-size", "25px");
  button.style("border","none");
  button.style("border-radius", "20px")
  //Agree button functions
  button.mouseOver(changeColor); //Hovering mouse over changes color
  button.mouseOut(resetColor); //No long hovering resets color
  button.mousePressed(dataScene); //Changes the scene to sceneNum 1

  //Mic set up
  mic = new p5.AudioIn();
  mic.start();
  
  //Video set up
  capture = createCapture(VIDEO);
  capture.size(220, 200);
  capture.hide();

  //set up for the disagree button
  x = 565; //Disagree cords
  y = 555; //Disagree cords
  disagreeSize1 = 180; //Button's width
  disagreeSize2 = 70; //Button's height
}

function draw() {
  //Allows to create different scenes, sceneNum is a variable set at 0
  switch(sceneNum){
    //First scene - Terms of Service
    case 0: //Everything in between case 0: and break; will show when sceneNum is 0
      background(255);
      image(bg1, 0, 0); //Loads in the shadow figure
      //Box for TOS (Terms of Service)
      strokeWeight(1);
      stroke(0);
      fill(255);
      rect(200, 240, 600, 280);
      noStroke();
      //Title and call for action
      fill(0);
      textAlign(CENTER);
      textSize(60);
      text("Welcome", width/2, 150);
      textSize(30); 
      text("Please Agree to The Terms of Service Before We Proceed", width/2, 200);
      //TOS text
      textAlign(LEFT);
      textSize(10);
      text(tos, 210, 250, 580, 260); //Loads in the variable, and 210, and 250 determines top left corner of the box while 580 and 260 is the bottom right corner.
      
      //Calculates the distance between the mouse and the variables x and y that will be used for the agree button.
      let d = dist(mouseX, mouseY, x, y);
      //Conditional statements that ensures when the distance is smaller than the width and height of the button, x and y will recieve new numbers.   
      if (d < (disagreeSize1)) { 
        x = random(width);
        y = random(height);
      }
      if (d < (disagreeSize2)) { 
        x = random(width);
        y = random(height);
      }
      //Disagree button
      strokeWeight(1);
      stroke(140);
      fill(255);
      rect(x, y, disagreeSize1, disagreeSize2, 20);
      noStroke();
      fill(140);
      textSize(25);
      text("Disagree", x+43, y+42);
    //Functions as a stop to the scene1
      break;

//Scene 2 - Data Capturing Scene
  case 1: //Everything in between case 1: and break; will show when sceneNum is 1
    background(255);
     image(bg1, 0, 0);
     soundBar(); //Draws the custom soundBar function
  
  //Draws the video
    image(capture, 70, 180, 220, 200);
  
//Data collection text, 3 different arrays of text
let faceWords = ["Collecting face...", "Tracking Face...", "Collecting eye color...", "Collecting lip movement...", "Collecting skintone...", "Selling data...", "Collecting smile...", "Collecting hair data...", "Collecting frown..."];
let voiceWords = ["Collecting voice...", "Collecting secrets...","Collecting cadence...", "Collecting tone...", "Collecting volume...", "Recording voice...", "Selling voice...", "Collecting pitch...", "Collecting language..."];
let comWords = ["Collecting browser history", "Collecting mouse clicks", "Collecting key clicks", "Collecting data...", "Collecting keystrokes...", "Collect typing speed...", "Collecting right clicks...", "Selling data", "Deleting privacy...", "Collecting vulnerabilities"];
//Variables for holding random indexes in the arrays
let word1 = random(faceWords);
let word2 = random(voiceWords);
let word3 = random(comWords);
//Draws the words on canvas
fill(0);
textSize(35);
text(word1, 350, 200);
text(word2, 350, 250);
text(word3, 350, 300);

//Mouseclick and keyboard counters
stroke(0);
fill(255);
//The boxes
rect(400, 450, 100, 100);
rect(600, 450, 100, 100);
fill(0);
//The numbers that cout
textSize(50);
textAlign(CENTER);
text(countMouse, 450, 517);
text(countKey, 650, 517);
//Text above boxes
textSize(20);
text("Mouse Clicks", 450, 440);
text("Key Clicks", 650, 440);


//Loading bar on bottom
textAlign(LEFT);
text("Loading...", 400, 590);
stroke(0);
fill(255);
rect(400, 600, 300, 20);
fill(0);
noStroke();
rect(400, 600, loadingBar, 20); //The loading bar itself, width is set at 0.
 loadingBar += 0.3; //Loadingbar will grow with 0.3
  if (loadingBar >= 300) {  //When the loading bar's width is larger or equal to 300, sceneNum will become 2 and show scene 3
   sceneNum++ 
 }
break; //Stops scene 2

  //Scene 3 - Thank you for contributing
    case 2: //Everything in between case 2: and break; will show when sceneNum is 2
    background(240, 0, 0);
    image(bg2, 0, 0);
    textAlign(CENTER);
    textSize(80);
    let thank = 'THANK YOU FOR YOUR CONTRIBUTION :)';
    text(thank, 40, 200, 900, 900);
    break;
  }
}
//Custom function for changing the color of the button when hovering
function changeColor(){
  button.style("background-color","#3278C1");
  button.style("background", "linear-gradient(to bottom, #3278C1 0%, #215D9D 100%)");
}

//Custom function for going back to intial color once mouse is not hovering
function resetColor(){
  button.style("background-color","#4491E2");
  button.style("background", "linear-gradient(to bottom, #4491E2 0%, #307CCB 100%)");
}

//When the button is pressed it'll remove all elements, start recording with the mic and sceneNum will become 1.
function dataScene(){
  removeElements();
  userStartAudio();
  sceneNum++;
}

//Custom function for the soundbar
function soundBar(){
  let volume = mic.getLevel();
  let mapVolume = map(volume, 0, 1, 0, 500);
  noStroke();
  fill(0,255,0);
  rect(110, 390, 20, 20+mapVolume);
  rect(135, 390, 20, 10+mapVolume);
  rect(160, 390, 20, 30+mapVolume);
  rect(185, 390, 20, 10+mapVolume);
  rect(210, 390, 20, 5+mapVolume);
  rect(235, 390, 20, 20+mapVolume);
  push();
  //Recording blinking
  blink = blink + 1 //Blink goes up by 1 everytime it's looped in draw()
  if (blink % 15 === 0){ //Everytime blink increments by 15, the condition is true.
  fill(255); //White
    circle(92, 400, 15, 15);
  } else {
  //Every other times the circle is red
    fill(255, 0, 0); //red
  circle(92, 400, 15, 15);
  pop();
}
}

//everytime mouse is pressed, countMouse counts with one
function mousePressed(){
  countMouse++
}

//everytime a key is pressed, countMouse counts with one
function keyPressed(){
  countKey++
}
