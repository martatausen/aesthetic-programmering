# Group members
Marta (This gitlab)
<br>
[Kristín](https://gitlab.com/kristintrang/aesthetic-programming/-/tree/main)
<br>
[Salomon](https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/tree/main)
<br>
[Nadja](https://gitlab.com/NadjaNaneva/aep/-/tree/main)
<br>

RUNME:  [link](https://martatausen.gitlab.io/aesthetic-programmering/FinalProject/index.html)
<br>
Source code:  [link](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/FinalProject/FaceGuesser.js)
<br>
Buttons.js: [link](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/FinalProject/buttons.js)
<br>
JSON file: [link](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/FinalProject/classification.json)
