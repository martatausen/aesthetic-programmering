///////////////////////////////////////

let sceneNum = 0; //Controlls what number case we are in. Start at 0 first

let loadingBar = 0;

let inputValue = false; //Checks if an input has been submitted 

let float = 230;
let speed = 1;

/////////////////////////////////////

/////////////////////////////////////
//All preloads
function preload() {
    titleBgm = loadSound("assets/feedthemachine.mp3");
    spookyBgm = loadSound("assets/test.mp3");
    webcamBgm = loadSound("assets/pixelperfect.mp3");
    click = loadSound("assets/click.wav");

    bg = loadImage("assets/title.png");
    pcman = loadImage("assets/pcman.gif");
    stars = loadImage("assets/stars.gif");
    frame = loadImage("assets/frame.png")

    bubbleFont = loadFont("assets/04B30.ttf");
    pixelFont = loadFont("assets/RetroGaming.ttf");

    classification = loadJSON("classification.json")
}

////////////////////////////////////
//Set up, everything is run once
function setup() {
    createCanvas(windowWidth, windowHeight);
    backgroundMusic(); //Starts music upon entering

    //Set ups video capture
    capture = createCapture(VIDEO);
    capture.size(windowWidth, windowHeight);
    capture.hide(); //Hides the capture for later

    //Set ups tracker
    ctracker = new clm.tracker();
    ctracker.init(pModel);
    ctracker.start(capture.elt);

    //Pulls from json file
    age = classification.age;
    race = classification.race;
    adjective = classification.adjective;
    gender = classification.gender;
    generator(); //Generates a description only once

    playButton(); //Draws the play button once
}

////////////////////////////////////
//Draw function, loops
function draw() {
    //Switch statement controls which scene we are currently in
    switch (sceneNum) {
        ////////////////
        //TITLE SCREEN!
        case 0:
            /////////////
            //BACKGROUND
            imageMode(CORNER);
            image(bg, 0, 0, windowWidth, windowHeight);
            image(stars, 25, 18, 1400, 660);

            ////////////////
            //TEXT INSIDE BUTTON
            fill('#FFFFF');
            textFont(bubbleFont);
            textSize(40);
            noStroke();
            text('PLAY', width / 2, height / 2 + 245);

            ////////////
            //TITLE
            stroke(255);
            strokeWeight(8);
            fill(223, 70, 175);
            textSize(90);
            textFont(bubbleFont);
            textAlign(CENTER);

            //If statement for floating title
            float = float + speed;

            if (float > 240 || float < 220) {
                speed = -speed;
            }

            //Text with title and float in height
            text("FACEGUESSER", width / 2, float);

            /////////////
            //ANIMATED PIXEL PERSON
            imageMode(CENTER);
            image(pcman, width / 2, height / 2);
            //TEXT BY ANIMATION
            textFont(pixelFont);
            strokeWeight(4);
            textSize(20);
            text("What are you?", width / 2, height / 2 - 100);
            text("Make sure your webcam is on!", width / 2, height / 2 + 110);

            break; //TITLE SCREEN END

        ////////////////////////
        //WEBCAM SCENE
        case 1:
            /////////////////
            //VIDEO CAPTURE
            background(255);
            imageMode(CORNER);
            image(capture, 0, 0, windowWidth, windowHeight);
            textFont(pixelFont); //Making sure the right font is applied for upcoming text

            ///////////////////////////
            //FRAME AROUND VIDEO CAPUTURE
            image(frame, 0, 0, windowWidth, windowHeight);

            ///////////////////////
            //LOADING BAR
            strokeWeight(3);
            fill(223, 70, 175);
            rect(width - 240, height - 120, loadingBar, 20); //The loading bar itself, width is set at 0.
            loadingBar += 0.3; //Loadingbar will grow with 0.3
            if (loadingBar >= 60) {  //When the loading bar's width is larger or equal to 300, it'll become case 2
                sceneNum = 2;
                yesButton(); //Draws the yes button once
                noButton(); //Draws the no button once
            }

            ///////////////////////
            //TRACKER WITH BOX WITH SUBMITTED WORDS
            if (inputValue === true) { //When user has submitted the following happens
                userInput = input.value(); //Assigns the text in the input to userInput
    
                let positions = ctracker.getCurrentPosition();
                if (positions.length) {
                    //Box around face
                    push();
                    noFill();
                    rectMode(CENTER);
                    stroke(200, 0, 200);
                    strokeWeight(10)
                    rect(positions[33][0] - 20, positions[33][1], 500, 500); //Box is set to be on bridge of the nose
                    //The text under the box
                    strokeWeight(2)
                    stroke(255);
                    fill(200, 0, 200);
                    text(userInput, positions[33][0] - 20, positions[33][1] + 300); //Writes the user input 
                    translate(windowWidth / 2, windowHeight / 2); //Ensures the facetracker is in correct place
                    pop();
                }
                //////////////////////
                //TRACKER WITH BOX WITH GENERATED WORDS
            } else { //When no input has been submitted
                let positions = ctracker.getCurrentPosition();

                if (positions.length) {
                    //Box around face
                    push();
                    noFill();
                    rectMode(CENTER);
                    stroke(200, 0, 200);
                    strokeWeight(10)
                    rect(positions[33][0] - 20, positions[33][1], 500, 500);
                    //The text under the box
                    strokeWeight(2);
                    stroke(255);
                    fill(200, 0, 200);
                    text(description, positions[33][0] - 20, positions[33][1] + 300);
                    //Check function generator(); to see what the description is comprised of
                    translate(windowWidth / 2, windowHeight / 2); //Ensures the facetracker is in the correct place
                    pop();
                }
            }
            break; //WEBCAM SCENE END

        //////////////////////////////////
        //IS THIS ACCURATE SCENE!
        case 2:
            ///////////////////
            //ACCURATE TEXT
            textSize(100);
            fill(223, 70, 175);
            strokeWeight(8);
            stroke(255);
            text("IS THIS ACCURATE?", width / 2, height / 2 - 40);

            /////////////
            //YES TEXT
            textSize(35);
            fill(255);
            noStroke();
            text("YES", width / 2 - 340, height / 2 + 195)

            ////////////
            //NO TEXT
            textSize(35);
            fill(255);
            noStroke();
            text("NO", width / 2 + 260, height / 2 + 195)

            break; //IS THIS ACCURATE SCENE END

        /////////////////////
        //SUBMIT SCENE
        case 3:

            //////////////////////
            //BACKGROUND
            background(0);

            //////////////////////
            //TEXT
            fill(255, 0, 0);
            noStroke();
            textSize(120);
            text("Classify yourself", random(width / 2 - 1, width / 2 + 1), random(height / 2 - 1, height / 2 + 1));

            ////////////////////
            //SUBMIT TEXT
            textSize(45);
            text("SUBMIT", random(width / 2 - 10, width / 2 - 11), random(height / 2 + 220, height / 2 + 221));

            break; //SUBMIT SCENE END

        ////////////////////////
        //THANK YOU SCENE
        case 4:

            /////////////////////
            //BACKGROUND
            background(0);
            fill(255, 0, 0);

            ////////////////////
            //TEXT
            noStroke();
            textSize(100);
            text("Thank you for your", random(width / 2 - 1, width / 2 + 1), random(height / 2 - 50, height / 2 - 51));
            textSize(220);
            text("FACE :)", random(width / 2 - 1, width / 2 + 1), random(height / 2 + 200, height / 2 + 201));

            ///////////////////////
            //TIMER TO GO BACK TO TITLE SCREEN
            if (frameCount === 100) {
                sceneNum = 0 //Goes back to case 0
                spookyBgm.stop();
                titleBgm.loop();
                loadingBar = 0; //Resets the loading bar

                //CREATES NEW PLAY BUTTON
                playButton();
            }

            break; //THANK YOU SCENE END

    }

}

/////////////////////////////////////
//STARTS TITLE MUSIC
function backgroundMusic() {
    titleBgm.loop();
    userStartAudio(); //Once the user interacts with the browser, audio can start
}

///////////////////////////
//RANDOMISES DESCRIPTIONS
function generator() {
    //Randomises the word that gets pulled from each array
    randomAge = age[int(random(age.length))];
    randomRace = race[int(random(race.length))]
    randomAdjective = adjective[int(random(adjective.length))]
    randomGender = gender[int(random(gender.length))]
    //The description that will be shown
    description = randomAdjective + " " + randomRace + " " + randomAge + " " + randomGender;
}


//CREDITS:
//Title music: "Feed the Machine" by Dream-Protocol from Pixabay
//Webcam music: "Pixel Perfect" by Lesiakower from Pixabay
//Classify music: "Pixel song #5" by hmmm101 from Pixabay
//Bubbly font: "04B-40.ttf" by 04
//Pixel font: "Retro Gaming.tff" by Daymarius
