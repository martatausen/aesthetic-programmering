# Welcome to my First MiniX Assignment!

![]() <img src="/minix1/miniXscreenshot.png"  width="400">

link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix1/index.html)
<br/>
link to:[Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix1/minix1.js)


# Description of my Program
In my program I've created a graphical drawing of a protrait of a man using shapes and background elements that fade in when you first view the program.

# What have I learnt in my first MiniX
There's a couple of things that I've learnt about programming while I was creating this graphical drawing. When I first set down to make a graphical depiction of a man, I quickly realised that my code could become exetremely muddled and easy to mix up different components of the shapes. To tackle this problem I decided I would label every single thing using // to make sure I would not for example mix the head with the eyes when I needed to adjust the parameters.

By using shapes for creating a drawing, I was able to familiarise myself with the different shape options that are available. The p5.js reference list was helpful in this regard of learning what different shapes there were, and how they functioned.
I quickly discovered that the shape would always start right where you set your x-cord, and it would always be right below your y-cord. Because I had set my canvas as `createCanvas(500,500)` I knew that going below 0 and going above 500 in the cords in the shape's paremeters, it would be out of view. The x-cords would go from left to right horizontally, at 0 it would be at the start of the canvas, and move further right the higher the number. With y-cords it goes from top to bottom vertically.

I found that doing this exercise I became a lot more aware of how I could place my cords, at first it was a very rigorous process of refreshing whenever I made a change to the cords of a shape, I became better over time at placing the shape where I wanted as I proceeded.

I figured out quickly that the newest string of code would overlap the older ones. Here you can see this in action with the head and eye shapes. The head comes first, and the eyes come second, this means that the head is under eyes.
```
//head
  ellipse(250,195,230,350);
  //left eye
  ellipse(195,180,50,20);
  //right eye
  ellipse(300,180,50,20);
  ```
Once I made the pupils I realised that I wanted them coloured, I went on the p5.js reference list and found `fill()` I solely wanted to colour the pupils at this point, and soon discovered that adding `fill()` just anywhere in the code would colour all the existing shapes in the code. For this reason I had to put `fill()` and the parameters in for the desired colours before any shapes I wanted with that colour.

When I was creating the body/shoulders, I didn't like the exetreme blocky nature of the rectangle I chose to use for it. Luckily in the p5.js references, I disovered that adding a 5th parameter in the `rect(x,y,w,h)` would make the corners of the rectangle rounded. I used this to my advantaged to create more natural looking shoulders, and a less blocky looking hat.

By pure accident I discovered the fade in option that I used for the background element. I was supposed to adjust one part of the hat, when I accidently had clicked on the `fill()` I had used for the hat's colour. This one was using RGB `fill(r,g,b)` and I added a fourth parameter to it, and when I checked the program after adding it, the hat's colour faded in. Confused I looked at the code, and saw I had added this fourth parameter to the code. I thought this was a cool feature, and decided to create a little background element where I added this feature to it.

# Reflection
Through this assignment I became more aware of there being an order in which things should be written in order for it to work as you want. It could be frustrating at time if you made a small typo, or forgot a parameter, cause it would break the code, and it would not function as it should. Coding needs precision, programming does have a lot of room for possibilites of creation, yet there are rules for it to function. The punishment of making a grammatical mistake or a small spelling mistake is massive in programming when compared to typical writing, where the reader can still discern what the message was, and at worst case not understand the sentence, while in programming a computer just does not have that kind of ability, making it very neccessary to be careful of mistakes like that. 

# References
Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017)
