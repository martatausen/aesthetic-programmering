let textBoxes = ["Loading...", "It'll be quick, I promise", "Almost there!", "...", "Just another second!!", "Soooo...", "Come here often?", "I'm almost done, I promise :)", "You're still here?"];
let word;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate (35); 
  //Text properties
  fill(200, 20, 20);
  textSize(15);
  textFont('Georgia');
  textAlign(CENTER);
  word = textBoxes[0]; //Assigns word to index 0 of textBoxes
  //Timing of the text appearance
  setTimeout(one, 5000); //custom function that assigns word to index 1, and then 5000 is how many milliseconds pass before the text appears
  setTimeout(zero, 9000);
  setTimeout(two, 15000);
  setTimeout(zero, 19000);
  setTimeout(three, 24000);
  setTimeout(four, 29000);
  setTimeout(three, 35000);
  setTimeout(five, 42000);
  setTimeout(six, 47000);
  setTimeout(three, 52000);
  setTimeout(seven, 58000);
  setTimeout(three, 64000);
  setTimeout(zero, 74000);
  setTimeout(eight, 100000);
  setTimeout(three, 108000);
  setTimeout(zero, 120000);
}
 function draw() {
background(200, 10); 
throbber();
text(word, width/2, height/2+120); //draws the text box as well keeps it in center and below the throbber
}
//Each index in the textBoxes array are assigned their own function
function zero(){
  word = textBoxes[0];
   } 
function one(){
word = textBoxes[1]
} 
function two(){
  word = textBoxes[2];
   } 
function three(){
  word = textBoxes[3];
   }    
function four(){
  word = textBoxes[4];
    } 
function five(){
   word = textBoxes[5];
     } 
function six(){
    word = textBoxes[6];
     } 
function seven(){
    word = textBoxes[7];
    } 
function eight(){
    word = textBoxes[8];
    } 
      
//New function of the throbber itself
 function throbber() {
 //number of shapes drawn for inner circles
let num = 50;
//number of shapes drawn for outer circles
let num2 = 3; 
 push();
  //Keeps throbber in center
 translate(width/2, height/2);
 //The num continously rotates a full 360 degrees 
 let cir = 360/num*(frameCount%num);
 rotate(radians(cir));
 noStroke();
 fill(150, 10, 10);
 ellipse(0, 0, 20, 2);
 fill(200, 20, 20);
 rect(20, 0, 5, 5);
 //The outer circles have a different num
 let cir2 = 360/num2*(frameCount%num2);
 rotate(radians(cir2));
 fill(60, 0, 0);
 rect(45, 0, 10, 10);
 fill(90, 0, 0);
 ellipse(70, 0, 50, 2);
 ellipse(200, 0, 90, 2);
 ellipse(400, 0, 200, 2);
 ellipse(700, 0, 250, 2);
 pop();
}
 
//Keeps everything consisent when the window is resized
 function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
 }
