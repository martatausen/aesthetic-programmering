# Group member's individual flowcharts
Link to: [Kristín's flowchart](https://gitlab.com/kristintrang/aesthetic-programming/-/tree/main/MiniX8)
<br>
Link to: [Salomon's flowchart](https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/tree/main/MiniX8)
<br>
Link to: [Nadja's flowchart](https://gitlab.com/NadjaNaneva/aep/-/tree/main/MiniX8)
<br>
# Marta's Individual Flowchart of Minix6
![]() <img src="/minix8/flowchartmarta.png" width="800">

# Group's flowcharts

## Idea One - AI Classification of People
Our first idea is a critical design that questions the bias in data. AI is fed datasets that are most often labeled by humans, and by default will reflect society’s assumptions and biases. In this idea, the user will be shown the prompts that describe their appearance and will quite literally be put in boxes. Although, in our program the classifications would be randomly generated (limited by technical skills). The user will then be asked if the classification is accurate, which if not, the user will be prompted to describe themselves. After this the program will end in that it thanks you for your contribution. Here we get into the idea of free labor as well.
<br/>
![]() <img src="/minix8/GROUP_FLOWCHART.jpg" width="900">


## Idea 2 - Webshop for Third Party companies
We often say our data is being sold, but what would that look like? We have created an imaginary vision of what a webshop for data would look like for third party buyers. Here you are the third party/company buying data from other companies like Facebook, TikTok, Instagram and like to market your own product to specific users. You can either directly pick a profile or you can browse between different categories of people. We wished to create an infinite shopping loop, that is why no matter if you continue to shop or not, you are led back to the main page of the website.
<br/>  
![]() <img src="/minix8/GROUP_FLOWCHART-2.jpg" width="900">

# Reflections
## What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?


It’s difficult to find the right kind of language that accurately describes the complexities of your program, while not making it too complicated for anyone to read and understand your ideas. There was definitely some restraint needed to avoid for the flowchart to become far too complicated especially in the website idea where there are a lot of different possible options to be clicked. It is easy going into the smallest detail, however we needed to keep it simple or it would have become far too overwhelming to comprehend.


## What are the technical challenges facing the two ideas and how are you going to address these?
One of the challenges was our own programming skills in the sense that we have limited experience in programming. The technical challenges are mostly how to make the website as simple as possible, but still have to have the categories and the profiles added to the website so it’s visible in the right side menu. The other technical challenge is with the webcam to have the describing words connected to the picture. The other challenging part is how to program a box and the keyboards so that you can write a sentence.


We can address these technical challenges by making plans on how we want our design to look. By doing so we have a visual plan to follow. We also have made flowcharts in this Minix so we have the flowcharts to take into consideration in how we want to program the ideas. While our ideas are not fully finalized yet we believe in their potential and that our final project is going to be on the same topic as the ideas are. 

As both ideas have different scenes, using `switch` statements would be immensely useful to keep code contained to specific scenes.

## In which ways are the individual and the group flowcharts you produced useful?
It created a foundation and made the group gather for a common interest/topic in programming. Further, it gives a good overview. Everyone is involved in the process, we essentially get a step-by-step “guide” so everyone knows what is to expect from the project. It also is a good tool to properly communicate our ideas in a way everyone can understand, and helps solidify vague ideas into a more concrete project that could be possible for our final project. Flowcharts are in general useful to understand the programming world and create a list of things you need to include in your design. 

