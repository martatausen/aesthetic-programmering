# Bottle Collector - AWSD controls!
![]() <img src="/minix6/screen1.png" width="600">
<br/>
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix6/index.html)
<br>
Link to: [Main Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix6/minix6.js)
<br>
Link to: [Player Class file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix6/player.js)
<br>
Link to: [Bottle Class file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix6/bottle.js)
<br>
# What have I produced?
I've made an overworld game where the player has to collect littered bottles in nature to "win", collecting all 6 bottles will prompt a screen thanking the player for helping nature out. All the drawn assets are created by me. There are two objects in this program, the `player class` and the `bottle class` and they're interactable with each other. There are bushes around giving an illusion of walls, however the player can walk pass them, as I ran out of time figuring out how to create wall collision with the player (A project for the minix7 perhaps?). The player is controlled by the `AWSD` keys, and pressing the `m` key will cause a song to play.

# What have I learnt?
This has by far been the minix I've struggled the most with. At first creating objects using class felt extremely abstract and difficult to understand what was happening. Having spent many hours on it now, it feels a lot less daunting, but there's still a lot of room to learn and improve upon. I wanted the player's sprite to be able to move in four different directions, and the method I used is not ideal but it works for the most part. I made the player a class, and after giving him x and y values in the constructor, I made a function called `show()` for him. In this function I've assigned a gif of the sprite whenever each key is held down, so fx. if `A` is down, it would show the sprite facing left. And when no key is held down, it shows the still front facing sprite. I soon ran into issue here, because the still front facing sprite would still appear while the other keys were held down. I did this to solve this issue:
```
push();
        if(keyIsPressed === false){
        image(char[0], this.xpos, this.ypos, this.w, this.h);
        noStroke();
        noFill();
        }else{
            erase(); //deletes the frontfacing sprite while any key is pressed
        }
        pop();
```
What's happening here is that the still sprite will show when no key is being pressed, but if it is being pressed then the sprite will be erased. This isn't a perfect solution, because now whenever a key that isn't `AWSD` is held down, the player will disappear. As well we have the issue of if two `AWSD` keys are held down, it'll show both sprites of whatever key is being held. `push();` and `pop();` ensures that `erase();` will only take effect in this one instance.
<br/>
Now for the part I struggled the most with and ate up the majority of my time working on this, was figuring out how to let the player pick up the bottles. I ran into issues, because unlike the tofu game, these bottles are hardcoded to appear in the same spot vs. random spots. The bottles are an array, and each index has been assigned as a `new Bottle` in the `setup()`, I started running into issues with this once I made `if statements` involving the distance between the player and the bottle, and had set it so that the bottles would splice. But, whenever I tried this it would erase both the player and the bottle, because it could no longer read `bottle[0].show();`, so I tried putting it in the `if statement` itself, but then it would continue trying to find the distance variable and would break the program. In the end, although not a perfect solution, I ended up creating a new function the the `Bottle class` called `removed();`, which would just change the x value of the bottle to -1000, effectively not deleting the object but rather moving it off screen.
```
  let d0;
    d0 = dist(player.xpos, player.ypos, bottle[0].xpos, bottle[0].ypos); //Distance between bottle[0] and player
  
  if(d0 < player.w/2){ 
    bottle[0].removed(); //xpos on bottle moves off-screen, bottle[0] isn't actually deleted
    score++
    }else{
      bottle[0].show();
    }
```
I ended up hardcoding every bottle index to have their own distance and if statements, where if the player touches the bottle the `removed();` function would take action, as well score increases by one, which allows for the score in the top left. For the win condition, it's a simple `if statement` where once `score === 6`, the program a text appears and the program stops looping.

# Reflection
Typically games have objections where you either have to kill to win or avoid being killed or both, I wanted to break this convention completely by making a low intense game with no lose condition. The "win" condition if you will, functions much more like a call to action than anything. It encourages you to take care of nature and pick up trash when you see it, although it's a very idyllic way to look at a solution to climate change that hangs over our heads. It's also ironic considering the bottle doesn't actually disappear when you pick it up in the code, it still exists offscreen taking up space in the data. As well the player has an unrealistic appearance, you can tell he's a human, but no human looks like him, and the bottles are almost bigger than him. The bushes give you a slight depth perception, you'd assume you can't pass through them, but alas once you walk over it your entire perception is completely destroyed, and all suspension of disbelief that this is a "real" world is destroyed. I could have easily taken the bushes out of the program, but I thought this effect you receive is pretty interesting when you consider the abstractions that object-oriented programming brings, it almost completely brings you out of the experience and gives you a reality check that the images you're seeing on the screen really are just some flat images being processed by a machine in binary code. Object oriented programming feels very different than from what we've previously been working with. It feels a lot more unstructured and nonlinear, the focus becomes the objects and their interactivity to each other vs. the computer just running through a linear sequence. Although I used the word unstructured, it still gives you the ability to organise better with the use of several files vs. having a long code consisiting of functions and logic. If I needed to change something with the bottle for example, I could just easily go into the `bottle class` file, and it would affect every single bottle drawn on the canvas.

# References
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
<br/>
Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).

## music
"Pleasant Porridge" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 4.0 License
http://creativecommons.org/licenses/by/4.0/
