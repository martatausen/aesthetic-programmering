# Welcome to my minix5!
![]() <img src="/minix5/screen1.png" width="600">
<br/>
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix5/index.html)
<br>
Link to: [Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix5/minix5.js)
<br>
# What have I produced?
The base of my miniX is the classic 10 Print code, with new rules added to it giving it an entirely different look from the original 10 Print. Here are the rules of the program:
```
///////////////////////////////////////
//Rules of the program
//Two lines are drawn at a time
//One line is either vertical or horizontal
//The other line is either diagonal forward or backwards
//The program will draw these lines horizontally until whole canvas is filled
//100 random circles are continuously drawn starting at (0, 10) cords and increases down the canvas 
```
What makes this program different from 10 Print, is that it draws two lines at a time, a random diagonal and a random vertical/horizontal, as well 100 random circles are drawn continuously with the range they can be drawn increasing over time, giving it a gradient look as the range increases and allows for more random chance for the circles to be drawn.
<br/>
# What have I learnt?
The program consists of 2 conditional statements and a forloop, as well 3 variables I've created. These variables are x, y and spacing. x and y are set to be 0, while spacing is at 20. Here is the first conditional statement as well some set up in `draw()`
``` 
const myRandom = random(1); //Choses a random number 0.0 to 1.0
 stroke(200, 100, 0); //Orange colour for lines
 strokeWeight(1);
 //Conditional statement for diagonal lines
 if(myRandom < 0.5){
  //If myRandom is smaller than 0.5, then it draws backslash
  line(x, y, x+spacing, y+spacing);
 }else{
  //If myRandom is bigger than 0.5, then it draws forwardslash
  line(x, y+spacing, x+spacing, y);
  ```
Here I'm creating a constant (const), which is a value that cannot be changed unlike variables. This const is set to be `myRandom = random(1);`, which then will be used in the conditional statement to decide if the line should be a forwardslash or backwardslash, and it does this by determining if the value of myRandom is either smaller of larger than 0.5.
If it's smaller then it draws a backslash, otherwise it's a forwardslash. I then made a secondary conditional statement with the same requirements, but changed cords in the line. To understand exactly what's happening in the coordinates of the lines, I drew a diagram showcasing exactly what's happening:
<br/>
![]() <img src="/minix5/illu1.png" width="600">

Now to make it move forward and start a new row of lines, we have this little string of code:
```
 x += spacing; //x = x+20; Ensures x will move 20 spaces forward
 if(x > width){ //If x reaches the end of canvas then... 
  y = y + spacing; //y should move 20 down
  x = 0; //Starting position of x
```
Because the line coordinates use x and y, it will affect the entire code when x is made to hold increase with 20 each time the code is ran, then we have a conditional statement that checks for when x reaches the end of the canvas, y will increase with 20, and x will have a starting position of 0 again, and x will run along the canvas once again until there's no canvas space anymore.
<br/>
Finally we have the forloop:
```
for(let i = 0; i < 100; i++){ //counts up to a 100 at a time 
    noStroke();
    fill(200, 100, 0, 70); //Alpha makes the circle fade in
    /*Circles are drawn randomly wherever x is, and starts at y+spacing: (0, 20), 
    and will increasingly increase the space it can be drawn vertically over the canvas*/
    circle(random(x), random(y+spacing), 1); 
```
Where there are continuously 100 circles drawn at a time, the colour has an alpha value which gives it a fade in effect. At the start the circles will be much more prominent, because they circles will only be drawn wherever the x currently is at, and at the start y only reaches down to 20. This means the range is small and as it increases the circles will spread out. Because the x resets, this means there's more chance for circles to be drawn in the left side versus far right where it will be possible for a few milliseconds. This will give the background a gradient effect concentrated in the top left corner. If left to run, then eventually the circles would cover the entire canvas, but it would take a long time as the circles are tiny.
<br/>
# Reflection
Technically the program actually doesn't have any ending, although we might perceive it as ending once the entire canvas has been filled with lines, however the reality is that the circles are still being drawn and will be drawn forever. But, because the circles are so small, we might not notice that they are in fact still appearing over the canvas, it's not as apparent as the fast lines filling the canvas with a perceived end to it. You will get a different look you get every time you run the program, but there will emerge patterns because of the rules making it not entirely random how the "finished" generative art will look like. This is emergent behaviour where natural patterns start to emerge from homogeneous uniform, we start seeing the same kind of shapes popping up several instances throughout the entire canvas from the four different possible line directions. The circles will always have a similar gradient look to it, even though it's entirely random where in the space they can be drawn will be drawn. Although it is random how it will look, I still have some control of the general look and feel it will have through the rules and colours I have chosen for it. 
